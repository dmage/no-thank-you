# Thank you, but no thank you!

> Tell recruiters and job hunters "no thank you" in a polite way.

## Usage

If someone is solicting you with job offers, just drop the link as an answer: https://no-thank-you.de/

## Contributing translations

1.  Fork the repository
2.  Copy one of the env files in `translations/`.
    Change the filename to the ISO 639-1 code of the language you are targeting.
    (e.g. French -> `fr.env`)
4.  Translate
3.  Create Merge Request
